# Prueba técnica

Hi Andrés,

This is my source code for the little layout exercise. I'll send you the content of the `dist/` folder by email as well.


## How to run the code on your computer

If you want to check the page live, clone that repo on your local machine and then, from the root, just run the following commands in a terminal:

`npm install`

And then:

`grunt`

And a browser window should open with the page itself (if you're not running anything on the port 8080).


## Disclaimer

There wasn't any requirements about browser support and any information about what content would be where so I didn't use any specific semantic and I only tested on *chrome* as well. I hope it's alright!


## Explanations

That was fun!

So usually, I'm a true believer of the mobile-first approach and that's the way I started this exercise as well.

I knew the transition from mobile to tablet was going to be easy but I also knew the transition from tablet to desktop would be slightly trickier. I hadn't figured it out when I started to be honest, but I wanted to give it a shot.
Well...I tried but couldn't find an elegant way of moving things around without using JS. And I sure didn't want to use JS for that, totally overkill and unnecesary.

So I decided to go the other way and make my page for desktop first. I changed the order of the divs in my html and it became much easier very quickly. A little bit of floats for desktop and then, once on tablet and mobile, everything is managed by flex (makes it super easy to change the order).

Please, Let me know if you have any questions!


## Interesting bits

The file you might be interested in are located here:

### html

`src/hbs/index.hbs`

### sass

`src/scss/index.scss`