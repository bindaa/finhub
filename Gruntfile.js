module.exports = function(grunt) {
	'use strict';

	// global variables
	var gruntConfig = {};

	// paths config
	var folderSrc 		= './src/',
		folderConfig	= './config/',
		folderDistDev 	= './dist/dev/',
		folderDistProd	= './dist/prod/',
		paths = {
			dev: folderSrc,
			config: folderConfig,
			build: {
				dev: folderDistDev,
				prod: folderDistProd
			},
			src: {
				styles: folderSrc + 'scss/',
				scripts: folderSrc + 'js/',
				libs: folderSrc + 'js/libs/',
				helpers: folderSrc + 'js/helpers/',
				pages: folderSrc + 'hbs/',
				partials: folderSrc + 'hbs/partials/'
			},
			dist: {
				dev: {
					styles: folderDistDev + 'css/',
					scripts: folderDistDev + 'js/',
					pages: folderDistDev
				},
				prod: {
					styles: folderDistProd + 'css/',
					scripts: folderDistProd + 'js/',
					pages: folderDistProd
				}
			}
		};




	/***********************************************/
	/* NPM
	/***********************************************/
	gruntConfig.pkg = grunt.file.readJSON('package.json');

	/***********************************************/
	/* WATCH CONFIG
	/***********************************************/
	gruntConfig.watch = {
		options: {
			livereload: true,
		},
		styles: {
			files: paths.src.styles + '**/*.scss',
			tasks: ['styles:dev']
		},
		scripts: {
			files: paths.src.scripts + '**/*.js',
			tasks: ['scripts:dev']
		},
		templates: {
			files: [
				paths.src.pages + '*.hbs',
				paths.src.pages + '*.json',
				paths.src.partials + '*.hbs',
				paths.src.partials + '*.json'
			],
			tasks: ['templates:dev']
		},
		grunt: {
			files: ['Gruntfile.js'],
			tasks: ['jshint:grunt'],
			options: {
				reload: true,
				livereload: false
			}
		}
	};

	/***********************************************/
	/* SASS CONFIG
	/***********************************************/
	gruntConfig.sass = {
		dev:{
			options: {
				precision: 5
			},
			files: [{
				cwd: paths.src.styles,
				dest: paths.dist.dev.styles,
				expand: true,
				ext: '.css',
				src: ['*.scss']
			}]
		},
		prod:{
			options: {
				outputStyle: 'compressed',
				precision: 5
			},
			files: [{
				cwd: paths.src.styles,
				dest: paths.dist.prod.styles,
				expand: true,
				ext: '.css',
				src: ['*.scss']
			}]
		}
	};


	/***********************************************/
	/* POSTCSS CONFIG
	/***********************************************/
	gruntConfig.postcss = {
		options: {
			processors: [
				require('autoprefixer')({browsers: ['last 3 versions', '>1%']})
			]
		},
		dev: {
			src: paths.dist.dev.styles + '*.css'
		},
		prod: {
			src: paths.dist.prod.styles + '*.css'
		}
	};


	/***********************************************/
	/* JSHINT CONFIG
	/***********************************************/
	gruntConfig.jshint = {
		options: {
			reporter: require('jshint-stylish')
		},
		dev: {
			// only validate our scripts, not the libs
			src: [paths.src.scripts + '**/*.js','!' + paths.src.libs + '**/*.js'],
			options: {
				bitwise: true,
				curly: true,
				eqeqeq: true,
				freeze: false,
				funcscope: false,
				latedef: "nofunc",
				nonew: true,
				strict: true,
				globals: {
					jQuery: true,
					define: true,
			        require: true,
			        requirejs: true,
			        "document": true,
			        "console": true,
			        "window": true,
			        "history": true,
			        "location": true,
			        "$": true
				}
			}
		},
		prod: {
			// only validate our scripts, not the libs
			src: [paths.src.scripts + '**/*.js','!' + paths.src.libs + '**/*.js'],
			options: {
				bitwise: true,
				curly: true,
				eqeqeq: true,
				freeze: false,
				funcscope: false,
				latedef: "nofunc",
				nonew: true,
				strict: true,
				undef: true,
				unused: true,
				globals: {
					jQuery: true,
					define: true,
			        require: true,
			        requirejs: true,
			        "document": true,
			        "console": true,
			        "window": true,
			        "history": true,
			        "location": true,
			        "$": true
				}
			}
		},
		grunt: {
			src: ['Gruntfile.js']
		}
	};




	/***********************************************/
	/* HANDLEBARS CONFIG
	/***********************************************/
	gruntConfig['compile-handlebars'] = {
		dev: {
			files: [{
				expand: true,
				cwd: paths.src.pages,
				src: '*.hbs',
				dest: paths.dist.dev.pages,
				ext: '.html'
			}],
			postHTML: paths.config + 'livereload.html',
			helpers: paths.src.helpers + '*.js',
			partials: paths.src.partials + '*.hbs',
			templateData: paths.src.pages + '*.json'
		},
		prod: {
			files: [{
				expand: true,
				cwd: paths.src.pages,
				src: '*.hbs',
				dest: paths.dist.prod.pages,
				ext: '.html'
			}],
			helpers: paths.src.helpers + '*.js',
			partials: paths.src.partials + '*.hbs',
			templateData: paths.src.pages + '*.json'
		}
	};



	/***********************************************/
	/* LOCAL WEB SERVER CONFIG
	/***********************************************/
	gruntConfig.connect = {
		dev: {
			options: {
				port: 8080,
				hostname: '*',
				base: paths.dist.dev.pages,
				open: true
			}
		},
		prod: {
			options: {
				port: 8081,
				hostname: '*',
				base: paths.dist.prod.pages,
				open: true,
				keepalive: true
			}
		}
	};


	/***********************************************/
	/* CLEAN CONFIG
	/***********************************************/
	gruntConfig.clean = {
		dev: [folderDistDev],
		prod: [folderDistProd]
	};


	// Project configuration.
	grunt.initConfig(gruntConfig);

	// load all the grunt tasks installed in build/node_modules folder
	require('load-grunt-tasks')(grunt);

	// DEFAULT TASK
	grunt.registerTask('default',['dev', 'connect:dev', 'watch']);
	// DEV TASKS
	grunt.registerTask('dev',['clean:dev', 'styles:dev', 'templates:dev']);
	grunt.registerTask('styles:dev',['sass:dev', 'postcss:dev']);
	grunt.registerTask('templates:dev',['compile-handlebars:dev']);
	// PROD TASK
	grunt.registerTask('prod',['clean:prod', 'sass:prod', 'postcss:prod', 'compile-handlebars:prod', 'connect:prod']);

};


// TODO
// - svg icons automatically generated from a folder (see: https://github.com/filamentgroup/grunticon)
// - notifications (or logs in the console) - OPTIONAL