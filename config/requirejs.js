requirejs.config({
	baseUrl: '../src/js/',
	paths: {
		// LIBS
		'jquery': 'libs/jquery',

		// JS MODULES
		"Alert": 'components/Alert'

		// RUNTIME COMPONENTS
		//"Alert" : 'iife/Alert'
	}
});